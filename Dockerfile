FROM nginx:1.13.8
MAINTAINER aricrenzo@gmail.com

#Put source template
COPY src/index.html /tmp/index.html

#Place Entrypoint script
COPY src/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

#Set Default Environment Variables
ENV app_version "1.0"
ENV app_environment "Development"
ENV custom_message "Univac is an acronym for UNIVersal Automatic Computer"


EXPOSE 80
ENTRYPOINT ["./entrypoint.sh"]
