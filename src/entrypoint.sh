#!/bin/bash

#Entrypoint script for Univac App

#Place Environment Variables
envsubst < /tmp/index.html > /usr/share/nginx/html/index.html

#Start nginx
nginx -g 'daemon off;'
