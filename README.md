# Univac 2 Demo Containerized Application

Univac 2 is a demo application I wrote to demonstrate some common Docker and Kubernetes concepts.   The goal of this project is to quickly deploy a simple web application that will display messages based on the environment variables set within the container at runtime.

To acheive this, I am using an NGINX server that displays a static `index.html` page.   When the container starts, the entrypoint script executes `envsubst` at run time, inserting the environment variables into the HTML page.  A simple demonstration of this is as follows:

1. Use Docker to build the container and run the app:

    ```
    univac_2$ docker build -t univac2:1.0 .
    univac_2$ docker run -p 80:80 -it -d univac2:1.0
    ```

    **NOTE**: The Univac web interface can be viewed in your web browser at: http://localhost:80/

2. Demonstrate modifying the environment variables in `docker run` syntax:

    ```
    univac_2$ docker stop %container_name%
    univac_2$ docker run -p 80:80 -e app_version=2.0 -e app_environment="Staging" -e custom_message="Enough Defaults. Time for Business" -it -d univac2:1.0
    ```

    **NOTE:** Refreshing your web browser will show the changes without requiring a code change or rebuilding the Docker image.

3. Deploy the app in Kubernetes using the included Helm chart and override the environment variables in `values.yaml`
